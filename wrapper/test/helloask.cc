#include "run.h"
#include "window.h"
#include "group.h"
#include "widget.h"
#include "style.h"
#include "ask.h"
#include <stdlib.h>

void hello(Widget *w, void *data)
{
  if(fl_ask_ask("bla"))
    {
      exit(0);
    }
}

int main(int argc , char **argv)
{
  Window* window;
  Widget* box;

  window = new_window(300,180, "bla");
  callback(window, hello, 0);
  fl_group_begin(window);

  box = new_widget(20, 40, 260, 100, "Hello, World!");
  fl_widget_box(box, __UP_BOX);
  fl_widget_labelfont(box, __HELVETICA_BOLD_ITALIC);
  fl_widget_labelsize(box, 36);
  fl_widget_labeltype(box, __SHADOW_LABEL);

  fl_group_end(window);
  fl_window_show(window, argc, argv);

  fl_run();
  return 0;
}
