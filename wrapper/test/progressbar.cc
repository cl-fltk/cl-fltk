#include "run.h"
#include "window.h"
#include "group.h"
#include "widget.h"
#include "style.h"
#include "progressbar.h"

Window* w;

static void ptimer(void *o)
{
	ProgressBar *pbar = (ProgressBar *)o;
	if(pbar->position() < 100)
	{
	  pbar->step(1);
		add_timeout(0.1, ptimer, (void *)pbar);
	}
	else
		w->hide();
}

int main(int argc, char **argv) {
  ProgressBar* pbar;
  {
    Window* o = new_window(400, 100);
    o->begin();
    w = o;
    { ProgressBar* o = new_progressbar(25, 25, 330, 25, "Simple Progress Bar");
      pbar = o;
      fl_widget_box(o, ENGRAVED_BOX);
      fl_widget_clear_flag(o, ALIGN_MASK);
      fl_widget_set_flag(o, ALIGN_BOTTOM);
      fl_widget_selection_color(o, BLUE);
      fl_widget_color(o, WHITE);
      fl_widget_textcolor(o, RED);
    }
    o->end();
  }
  add_timeout(0.1, ptimer, (void *)pbar);
  w->show(argc, argv);

  return run();
}
