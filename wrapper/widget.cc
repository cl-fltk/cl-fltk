#include "widget.h"

Widget*  new_widget(int x, int y, int width, int height, const char* text)
{
  debug("%s:%s %d, %d,  %d, %d, %s\n", __FILE__, __FUNCTION__, x, y, width, height, text);
  return new Widget(x, y, width, height, text);
}

void fl_widget_box(Widget* widget, Box* box)
{
  debug("%s:%s %x, %x\n", __FILE__, __FUNCTION__, (unsigned int)widget, (unsigned int)box);
  widget->box(box);
}

void fl_widget_labelfont(Widget* widget, Font* labelfont)
{
  debug("%s:%s %x, %x\n", __FILE__, __FUNCTION__, (unsigned int)widget, (unsigned int)labelfont);
  widget->labelfont(labelfont);
}

void fl_widget_labeltype(Widget* widget, LabelType* labeltype)
{
  debug("%s:%s %x, %x\n", __FILE__, __FUNCTION__, (unsigned int)widget, (unsigned int)labeltype);
  widget->labeltype(labeltype);
}

void fl_widget_labelsize(Widget* widget, float labelsize)
{
  debug("%s:%s %x, %f\n", __FILE__, __FUNCTION__, (unsigned int)widget, labelsize);
  widget->labelsize(labelsize);
}

void fl_widget_show(Widget *widget)
{
  debug("%s:%s %x\n", __FILE__, __FUNCTION__, (unsigned int)widget);
  widget->show();
}

void fl_widget_hide(Widget *widget)
{
  debug("%s:%s %x\n", __FILE__, __FUNCTION__, (unsigned int)widget);
  widget->hide();
}

int fl_widget_clear_flag(Widget *widget, int c)
{
  debug("%s:%s %x %i\n", __FILE__, __FUNCTION__, (unsigned int)widget, c);
  return widget->clear_flag(c);
}

int fl_widget_set_flag(Widget *widget, int c)
{
  debug("%s:%s %x %i\n", __FILE__, __FUNCTION__, (unsigned int)widget, c);
  return widget->set_flag(c);
}
 
int fl_widget_invert_flag(Widget *widget, int c)
{
  debug("%s:%s %x %i\n", __FILE__, __FUNCTION__, (unsigned int)widget, c);
  return widget->invert_flag(c);
}

void fl_widget_color(Widget *widget, int c)
{
  debug("%s:%s %x %i\n", __FILE__, __FUNCTION__, (unsigned int)widget, c);
  return widget->color(c);
}

void fl_widget_textcolor(Widget * widget, int c)
{
  debug("%s:%s %x %i\n", __FILE__, __FUNCTION__, (unsigned int)widget, c);
  return widget->textcolor(c);
}

void fl_widget_selection_color(Widget *widget, int c)
{
  debug("%s:%s %x %i\n", __FILE__, __FUNCTION__, (unsigned int)widget, c);
  return widget->selection_color(c);
}

void fl_widget_buttonbox(Widget* widget, Box* box)
{
  debug("%s:%s %x %x\n", __FILE__, __FUNCTION__, (unsigned int)widget, (unsigned int)box);
  return widget->buttonbox(box);
}

void fl_widget_focusbox(Widget* widget, Box* box)
{
  debug("%s:%s %x %x\n", __FILE__, __FUNCTION__, (unsigned int)widget, (unsigned int)box);
  return widget->focusbox(box);
}

void fl_widget_textfont(Widget* widget, Font* font)
{
  debug("%s:%s %x %x\n", __FILE__, __FUNCTION__, (unsigned int)widget, (unsigned int)font);
  return widget->textfont(font);
}

void fl_widget_selection_textcolor(Widget* widget, Color color)
{
  debug("%s:%s %x %i\n", __FILE__, __FUNCTION__, (unsigned int)widget, color);
  return widget->textcolor(color);
}

void fl_widget_buttoncolor(Widget* widget, Color color)
{
  debug("%s:%s %x %i\n", __FILE__, __FUNCTION__, (unsigned int)widget, color);
  return widget->buttoncolor(color);
}

void fl_widget_labelcolor(Widget* widget, Color color)
{
  debug("%s:%s %x %i\n", __FILE__, __FUNCTION__, (unsigned int)widget, color);
  return widget->labelcolor(color);
}

void fl_widget_highlight_color(Widget* widget, Color color)
{
  debug("%s:%s %x %i\n", __FILE__, __FUNCTION__, (unsigned int)widget, color);
  return widget->highlight_color(color);
}

void fl_widget_highlight_textcolor(Widget* widget, Color color)
{
  debug("%s:%s %x %i\n", __FILE__, __FUNCTION__, (unsigned int)widget, color);
  return widget->highlight_textcolor(color);
}

void fl_widget_textsize(Widget* widget, float a)
{
  debug("%s:%s %x %f\n", __FILE__, __FUNCTION__, (unsigned int)widget, a);
  return widget->textsize(a);
}

void fl_widget_leading(Widget* widget, float a)
{
  debug("%s:%s %x %f\n", __FILE__, __FUNCTION__, (unsigned int)widget, a);
  return widget->leading(a);
}

void fl_widget_scrollbar_align(Widget* widget, unsigned char c)
{
  debug("%s:%s %x %c\n", __FILE__, __FUNCTION__, (unsigned int)widget, c);
  return widget->scrollbar_align(c);

}

void fl_widget_scrollbar_width(Widget* widget, unsigned char c)
{
  debug("%s:%s %x %c\n", __FILE__, __FUNCTION__, (unsigned int)widget, c);
  return widget->scrollbar_width(c);
}

Color fl_widget_get_color(Widget* widget)
{
  return widget->color();
}

Color fl_widget_get_textcolor(Widget* widget)
{
  return widget->textcolor();
}

Color fl_widget_get_selection_color(Widget* widget)
{
  return widget->selection_color();
}

Color fl_widget_get_selection_textcolor(Widget* widget)
{
  return widget->selection_textcolor();
}

Color fl_widget_get_buttoncolor(Widget* widget)
{
  return widget->buttoncolor();
}

Color fl_widget_get_labelcolor(Widget* widget)
{
  return widget->labelcolor();
}

Color fl_widget_get_highlight_color(Widget* widget)
{
  return widget->highlight_color();
}

Color fl_widget_get_highlight_textcolor(Widget* widget)
{
  return widget->highlight_textcolor();
}

float fl_widget_get_labelsize(Widget* widget)
{
  return widget->labelsize();
}

float fl_widget_get_textsize(Widget* widget)
{
  return widget->textsize();
}

float fl_widget_get_leading(Widget* widget)
{
  return widget->leading();
}

unsigned char fl_widget_get_scrollbar_align(Widget* widget)
{
  return widget->scrollbar_align();
}

unsigned char fl_widget_get_scrollbar_width(Widget* widget)
{
  return widget->scrollbar_width();
}

int fl_widget_send(Widget* widget, int event)
{
  return widget->send(event);
}
