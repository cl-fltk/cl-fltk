#ifndef _CLFLTK_FL_H_
#define _CLFLTK_FL_H_

#include "cl-fltk.h"

#include <fltk/run.h>

using namespace fltk;

#include <fltk/Widget.h>

extern "C" {
  void callback(Widget* w, Callback* c, void* p) ;
  void fl_run();
  void fl_run_add_timeout(float t, TimeoutHandler*, void* v);
}

#endif 
