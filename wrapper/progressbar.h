#ifndef _CLFLTK_PROGRESSBAR_H_
#define _CLFLTK_PROGRESSBAR_H_

#include "fltk/ProgressBar.h"

using namespace fltk;

extern "C" {
  ProgressBar* new_progressbar(int x, int y, int w, int h, const char *lbl);
  void fl_progressbar_range(ProgressBar* pb, double min, double max, double step);
  void fl_progressbar_step(ProgressBar* pb, double step);
  double fl_progressbar_get_minimum(ProgressBar* pb);
  double fl_progressbar_get_maximum(ProgressBar* pb);
  void fl_progressbar_minimum(ProgressBar* pb, double nm);
  void fl_progressbar_maximum(ProgressBar* pb, double nm);
  double fl_progressbar_get_position(ProgressBar* pb);
  double fl_progressbar_get_step(ProgressBar* pb);
  void fl_progressbar_position(ProgressBar* pb, double pos);
  void fl_progressbar_showtext(ProgressBar* pb, bool st);
  bool fl_progressbar_get_showtext(ProgressBar* pb);
  void fl_progressbar_text_color(ProgressBar* pb, Color col);
  Color fl_progressbar_get_text_color(ProgressBar* pb);
}
#endif

