#ifndef _CLFLTK_ASK_H_
#define _CLFLTK_ASK_H_

#include "cl-fltk.h"

#include <fltk/ask.h>

using namespace fltk;

extern "C" int fl_ask_ask(char*);

#endif 
