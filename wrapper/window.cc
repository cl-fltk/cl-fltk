#include "window.h"

void fl_window_show(Window* window, int p1, char** p2)
{
  debug("%s:%s %x, %d, %s\n", __FILE__, __FUNCTION__, (unsigned int)window, p1, *p2);
  window->show(p1, p2);
}

Window*  new_window(int p1,int p2, const char* p3)
{
  debug("%s:%s %d, %d, %s\n", __FILE__, __FUNCTION__, p1, p2, p3);
  return new Window(p1,p2,p3);
}
