#ifndef _CLFLTK_BUTTON_H_
#define _CLFLTK_BUTTON_H_

#include "cl-fltk.h"

#include <fltk/Button.h>

using namespace fltk;

extern "C" Button* new_button(int x,int y,int w,int h,const char *text);

#endif 
