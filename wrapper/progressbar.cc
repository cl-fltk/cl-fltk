#include "progressbar.h"
#include <stdio.h>

ProgressBar* new_progressbar(int x, int y, int w, int h, const char *lbl = 0)
{
  return  new ProgressBar(x, y, w, h, lbl);
}
 
void fl_progressbar_range(ProgressBar* pb, double min, double max, double step = 1)
{
  pb->range(min, max, step);
}

void fl_progressbar_step(ProgressBar* pb, double step)
{
  pb->step(step);
}

double fl_progressbar_get_minimum(ProgressBar* pb)
{
  return pb->minimum();
}

double fl_progressbar_get_maximum(ProgressBar* pb)
{
  return pb->maximum();
}

void fl_progressbar_minimum(ProgressBar* pb, double nm)
{
  pb->minimum(nm);
}

void fl_progressbar_maximum(ProgressBar* pb, double nm)
{
  pb->maximum(nm);
}

double fl_progressbar_get_position(ProgressBar* pb)
{
  return pb->position();
}

double fl_progressbar_get_step(ProgressBar* pb)
{
  return pb->step();
}

void fl_progressbar_position(ProgressBar* pb, double pos)
{
  pb->position(pos);
}

void fl_progressbar_showtext(ProgressBar* pb, bool st)
{
  pb->showtext(st);
}

bool fl_progressbar_get_showtext(ProgressBar* pb)
{
  return pb->showtext();
}

void fl_progressbar_text_color(ProgressBar* pb, Color col)
{
  pb->text_color(col);
}

Color fl_progressbar_get_text_color(ProgressBar* pb)
{
  return pb->text_color();
}
