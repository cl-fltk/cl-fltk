#include "run.h"

void callback(Widget *w, Callback* c, void* p) 
{
  debug("%s:%s: %x, %x, %x\n", __FILE__, __FUNCTION__, (unsigned int)w, (unsigned int)c, (unsigned int)p);  
  w->callback(c,p);
  }

void fl_run()
{
  debug("%s:%s\n", __FILE__, __FUNCTION__);
  run();
}

void fl_run_add_timeout(float t,TimeoutHandler* th, void* v=0)
{
  debug("%s:%s: %f, %x, %x\n", __FILE__, __FUNCTION__, t, (unsigned int)th, (unsigned int)v);  
  add_timeout(t, *th, v);
}
