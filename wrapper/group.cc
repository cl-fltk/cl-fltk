#include "group.h"

void fl_group_end(Group *group)
{
  debug("%s:%s %x\n", __FILE__, __FUNCTION__, (unsigned int) group);
  group->end();
}

void fl_group_begin(Group *group)
{
  debug("%s:%s %x\n", __FILE__, __FUNCTION__, (unsigned int) group);
  group->begin();
}
