#ifndef _CLFLTK_WINDOW_H_
#define _CLFLTK_WINDOW_H_

#include "cl-fltk.h"

#include <fltk/Window.h>

using namespace fltk;

extern "C" Window*  new_window(int p1,int p2, const char* p3= 0);

extern "C" void fl_window_show(Window* window, int p1, char** p2);

#endif 
