#include "button.h"

Button* new_button(int x,int y,int w,int h,const char *text)
{
  debug("%s:%s %d, %d, %d, %d, %s\n", __FILE__, __FUNCTION__, x, y, w, h, text);
  return new Button(x, y, w, h, text);
}

