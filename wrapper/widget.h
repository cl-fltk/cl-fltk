#ifndef _CLFLTK_WIDGET_H_
#define _CLFLTK_WIDGET_H_

#include "cl-fltk.h"

#include <fltk/Widget.h>
#include <fltk/Style.h>

using namespace fltk;

extern "C" {
  Widget*  new_widget(int x, int y, int width, int height, const char* text= 0);

  void fl_widget_box(Widget* widget, Box* box);
  void fl_widget_labelfont(Widget* widget, Font* labelfont);
  void fl_widget_labeltype(Widget* widget, LabelType* labeltype);
  void fl_widget_labelsize(Widget* widget, float labelsize);
  void fl_widget_show(Widget* widget);
  void fl_widget_hide(Widget *widget);
  int fl_widget_clear_flag(Widget *widget, int c);
  int fl_widget_set_flag(Widget *widget, int c);
  int fl_widget_invert_flag(Widget *widget, int c);
  void fl_widget_color(Widget *widget, int c);
  void fl_widget_textcolor(Widget * widget, int c);
  void fl_widget_selection_color(Widget *widget, int c);
  void fl_widget_buttonbox(Widget* widget, Box*);
  void fl_widget_focusbox(Widget* widget, Box*);
  void fl_widget_textfont(Widget* widget, Font*);
  void fl_widget_selection_textcolor(Widget* widget, Color);
  void fl_widget_buttoncolor(Widget* widget, Color);
  void fl_widget_labelcolor(Widget* widget, Color);
  void fl_widget_highlight_color(Widget* widget, Color);
  void fl_widget_highlight_textcolor(Widget* widget, Color);
  void fl_widget_textsize(Widget* widget, float a);
  void fl_widget_leading(Widget* widget, float a);
  void fl_widget_scrollbar_align(Widget* widget, unsigned char);
  void fl_widget_scrollbar_width(Widget* widget, unsigned char);

  Color fl_widget_get_color(Widget* widget);
  Color fl_widget_get_textcolor(Widget* widget);
  Color fl_widget_get_selection_color(Widget* widget);
  Color fl_widget_get_selection_textcolor(Widget* widget);
  Color fl_widget_get_buttoncolor(Widget* widget);
  Color fl_widget_get_labelcolor(Widget* widget);
  Color fl_widget_get_highlight_color(Widget* widget);
  Color fl_widget_get_highlight_textcolor(Widget* widget);
  float fl_widget_get_labelsize(Widget* widget);
  float fl_widget_get_textsize(Widget* widget);
  float fl_widget_get_leading(Widget* widget);
  unsigned char fl_widget_get_scrollbar_align(Widget* widget);
  unsigned char fl_widget_get_scrollbar_width(Widget* widget);
  int fl_widget_send(Widget* widget, int event);

  /*
  const Style* style() const { return style_; }
  void  style(const Style* s) { style_ = s; }
  void  style(const Style& s) { style_ = &s; }
  bool  copy_style(const Style* s);
  static NamedStyle* default_style;
  static Symbol* default_glyph;

  Group* parent() const { return parent_; }
  void  parent(Group* w)        { parent_ = w; }
  Window* window() const        ;

  uchar type() const            { return type_; }
  void  type(uchar t)           { type_ = t; }
  bool  is_group() const        { return type_ >= GROUP_TYPE; }
  bool  is_window() const       { return type_ >= WINDOW_TYPE; }

  bool  resize(int x,int y,int w,int h);
  bool  position(int x, int y)  ;
  bool  resize(int w, int h)    ;
void  get_absolute_rect( Rectangle *rect ) const;

  const char* label() const     { return label_; }
  void  label(const char* a);
  void  copy_label(const char* a);

  const Symbol* image() const   { return image_; }
  void  image(const Symbol* a)  { image_ = a; }
  void  image(const Symbol& a)  { image_ = &a; }

  const char *tooltip() const   { return tooltip_; }
  void  tooltip(const char *t)  { tooltip_ = t; }

  unsigned shortcut() const     ;
  bool  shortcut(unsigned key)  ;
  bool  add_shortcut(unsigned key);
  bool  remove_shortcut(unsigned key);
  bool  remove_shortcuts()      ;
  unsigned label_shortcut() const;
  bool  test_label_shortcut() const;
  bool  test_shortcut() const   ;
  bool  test_shortcut(bool) const;

  Callback_p callback() const   { return callback_; }
  void  callback(Callback* c, void* p) { callback_=c; user_data_=p; }
  void  callback(Callback* c)   { callback_=c; }
  void  callback(Callback0*c)   { callback_=(Callback*)c; }
  void  callback(Callback1*c, long p=0) { callback_=(Callback*)c; user_data_=(void*)p; }
  void* user_data() const       { return user_data_; }
 void  user_data(void* v)      { user_data_ = v; }
  long  argument() const        { return (long)user_data_; }
  void  argument(long v)        { user_data_ = (void*)v; }
  uchar when() const            { return when_; }
  void  when(uchar i)           { when_ = i; }

  static void default_callback(Widget*, void*);
  void  do_callback()           { callback_(this,user_data_); }
  void  do_callback(Widget* o,void* arg=0) { callback_(o,arg); }
  void  do_callback(Widget* o,long arg) { callback_(o,(void*)arg); }
  bool  contains(const Widget*) const;
  bool  inside(const Widget* o) const { return o && o->contains(this); }
  bool  pushed() const          ;
  bool  focused() const         ;
  bool  belowmouse() const      ;

  Flags flags() const           { return flags_; }
  Flags flags(Flags f)          { return flags_ = f; }
  Flags set_flag(int c)         { return flags_ |= c; }
  Flags clear_flag(int c)       { return flags_ &= ~c; }
  Flags invert_flag(int c)      { return flags_ ^= c; }

  Flags align() const           { return flags_&ALIGN_MASK; }
  void  align(unsigned a)       { flags_ = (flags_ & (~ALIGN_MASK)) | a; }
  bool  visible() const         { return !(flags_&INVISIBLE); }
  bool  visible_r() const       ;
  void  show()                  ;
  void  hide()                  ;
  void  set_visible()           { flags_ &= ~INVISIBLE; }
  void  clear_visible()         { flags_ |= INVISIBLE; }
  bool  active() const          { return !(flags_&NOTACTIVE); }
  bool  active_r() const        ;
  void  activate()              ;
  void  activate(int b)         { if (b) activate(); else deactivate(); }
  void  deactivate()            ;
  bool  output() const          { return (flags_&OUTPUT)!=0; }
  void  set_output()            { flags_ |= OUTPUT; }
  void  clear_output()          { flags_ &= ~OUTPUT; }
  bool  takesevents() const     { return !(flags_&(OUTPUT|INVISIBLE|NOTACTIVE)); }
  bool  changed() const         { return (flags_&CHANGED)!=0; }
  void  set_changed()           { flags_ |= CHANGED; }
  void  clear_changed()         { flags_ &= ~CHANGED; }
  bool  value() const           { return (flags_&VALUE)!=0; }
  void  set_value()             { flags_ |= VALUE; }
  void  clear_value()           { flags_ &= ~VALUE; }
  bool  selected() const        { return (flags_&SELECTED)!=0; }
  void  set_selected()          { flags_ |= SELECTED; }
  void  clear_selected()        { flags_ &= ~SELECTED; }
  bool  click_to_focus()        { return (flags_ & CLICK_TO_FOCUS) != 0; }
  void  set_click_to_focus()    { flags_ |= CLICK_TO_FOCUS; }
  void  clear_click_to_focus()  { flags_ &= ~CLICK_TO_FOCUS; }
  bool  tab_to_focus()          { return (flags_ & TAB_TO_FOCUS) != 0; }
  void  set_tab_to_focus()      { flags_ |= TAB_TO_FOCUS; }
  void  clear_tab_to_focus()    { flags_ &= ~(TAB_TO_FOCUS|CLICK_TO_FOCUS); }
  bool  horizontal() const      { return !(flags_&LAYOUT_VERTICAL);}
  bool  vertical() const        { return (flags_&LAYOUT_VERTICAL)!=0;}
  void  set_horizontal()        { flags_ &= ~LAYOUT_VERTICAL; }
  void  set_vertical()          { flags_ |= LAYOUT_VERTICAL; }
  bool  take_focus()            ;
  void  throw_focus()           ;

  void  redraw()                ;
  void  redraw(uchar c)         ;
  void  redraw_label()          ;
  void  redraw_highlight()      ;
  void  redraw(const Rectangle&);
  uchar damage() const          { return damage_; }
  void  set_damage(uchar c)     { damage_ = c; } // should be called damage(c)

  void  relayout()              ;
  void  relayout(uchar damage)  ;
  uchar layout_damage() const   { return layout_damage_; }
  void  layout_damage(uchar c)  { layout_damage_ = c; }

  void  add_timeout(float)      ;
  void  repeat_timeout(float)   ;
  void  remove_timeout()        ;

  void  make_current() const    ;
  void  draw_background() const ;
  void  draw_frame() const      ;
  void  draw_box() const        ;
  void  draw_label() const      ;
  void  draw_label(const Rectangle&, Flags) const ;
  void  draw_glyph(int, const Rectangle&) const ;
  void  cursor(Cursor*) const   ;
  void  measure_label(int&, int&) const ;
  Box*  box()                   const;
  Box*  buttonbox()             const;
  Box*  focusbox()              const;
  Symbol* glyph()               const;
  Font* labelfont()             const;
  Font* textfont()              const;
  LabelType* labeltype()        const;

  void glyph(Symbol*)           ;
  */

}
#endif 
