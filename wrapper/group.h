#ifndef _CLFLTK_GROUP_CLASS_H_
#define _CLFLTK_GROUP_CLASS_H_

#include "cl-fltk.h"

#include <FL/Fl_Group.H>

using namespace fltk;

extern "C" void fl_group_end(Group *group);

extern "C" void fl_group_begin(Group *group);

#endif 
