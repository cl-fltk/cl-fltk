#ifndef _CLFLTK_H_
#define _CLFLTK_H_

#ifdef DEBUG
#include <stdio.h>
#define debug(...) fprintf(stderr, __VA_ARGS__)
#else
#define debug(...)
#endif

#endif
