#ifndef _CLFLTK_STYLE_H_
#define _CLFLTK_STYLE_H_

#include "cl-fltk.h"

#include <fltk/Widget.h>

using namespace fltk;

extern "C" Box *__UP_BOX;
extern "C" Box *__UP_BOX;
extern "C" Box *__DOWN_BOX;
extern "C" Box *__THIN_UP_BOX;
extern "C" Box *__THIN_DOWN_BOX;
extern "C" Box *__ENGRAVED_BOX;
extern "C" Box *__EMBOSSED_BOX;
extern "C" Box *__BORDER_BOX;
extern "C" Box *__FLAT_BOX;
extern "C" Box *__HIGHLIGHT_UP_BOX;
extern "C" Box *__HIGHLIGHT_DOWN_BOX;
extern "C" Box *__ROUND_UP_BOX;
extern "C" Box *__ROUND_DOWN_BOX;
extern "C" Box *__DIAMOND_UP_BOX;
extern "C" Box *__DIAMOND_DOWN_BOX;
extern "C" Box *__NO_BOX;
extern "C" Box *__SHADOW_BOX;
extern "C" Box *__ROUNDED_BOX;
extern "C" Box *__RSHADOW_BOX;
extern "C" Box *__RFLAT_BOX;
extern "C" Box *__OVAL_BOX;
extern "C" Box *__OSHADOW_BOX;
extern "C" Box *__OFLAT_BOX;
extern "C" Box *__BORDER_FRAME;
extern "C" Box *__DOTTED_FRAME;
extern "C" Box *__PLASTIC_UP_BOX;
extern "C" Box *__PLASTIC_DOWN_BOX;

extern "C" Font *__HELVETICA;
extern "C" Font *__HELVETICA_BOLD;
extern "C" Font *__HELVETICA_ITALIC;
extern "C" Font *__HELVETICA_BOLD_ITALIC;
extern "C" Font *__COURIER;
extern "C" Font *__COURIER_BOLD;
extern "C" Font *__COURIER_ITALIC;
extern "C" Font *__COURIER_BOLD_ITALIC;
extern "C" Font *__TIMES;
extern "C" Font *__TIMES_BOLD;
extern "C" Font *__TIMES_ITALIC;
extern "C" Font *__TIMES_BOLD_ITALIC;
extern "C" Font *__SYMBOL_FONT;
extern "C" Font *__SCREEN_FONT;
extern "C" Font *__SCREEN_BOLD_FONT;
extern "C" Font *__ZAPF_DINGBATS;

extern "C" LabelType *__NO_LABEL;
extern "C" LabelType *__NORMAL_LABEL;
extern "C" LabelType *__SYMBOL_LABEL;
extern "C" LabelType *__SHADOW_LABEL;
extern "C" LabelType *__ENGRAVED_LABEL;
extern "C" LabelType *__EMBOSSED_LABEL;

#endif
