#include "ask.h"

int fl_ask_ask(char* message)
{
  debug("%s:%s %s\n", __FILE__, __FUNCTION__, message);
  return ask(message);
}
