(defpackage #:cl-fltk-system
  (:use #:cl #:asdf))

(in-package #:cl-fltk-system)

(defsystem cl-fltk
  :description "CFFI Bindings For FLTK "
  :author "Dario Lah  <dlah@linux.hr>"
  :version "0.1.0"
  :licence "BSD"
  :perform (load-op :after (op cl-fltk)
                    (pushnew :cl-fltk cl:*features*))
  :components ((:module src
                        :components
                        ((:file "package")
			 (:file "cl-fltk" :depends-on ("package"))
                         (:file "run" :depends-on ("cl-fltk"))
                         (:file "widget" :depends-on ("cl-fltk"))
                         (:file "group" :depends-on ("widget"))
                         (:file "button" :depends-on ("widget"))
                         (:file "window" :depends-on ("group"))
			 (:file "style" :depends-on ("package"))
			 (:file "color" :depends-on ("package"))
			 (:file "flags" :depends-on ("package"))
			 (:file "progressbar" :depends-on ("widget"))
			 (:file "ask" :depends-on ("package")))))
  :depends-on (:cffi))
