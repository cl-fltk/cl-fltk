(require :asdf)
(asdf:operate 'asdf:load-op 'cffi)
(asdf:operate 'asdf:load-op 'cl-fltk)

(defvar window)
(defvar pbar)

(cffi:defcallback ptimer :void ((ignored :pointer))
		  (let ((position (fl:progressbar-position pbar)))
		    (if (or (not position)
			     (< position 100))
			(progn
			  (fl:progressbar-step pbar 1.0D0)
			  (fl:add-timeout 0.1 'ptimer (cffi:null-pointer)))
			(fl:hide window))))

(setf window (fl:new-window 400 100 "ProgressBar Demo"))

(fl:begin window)

(setf pbar (fl:new-progressbar 25 25 330 25 "Simple Progress Bar"))

(fl:box pbar fl:+ENGRAVED-BOX+)
(fl:clear-flag pbar fl:+ALIGN-MASK+)
(fl:set-flag pbar fl:+ALIGN-BOTTOM+)
(fl:selection-color pbar fl:+BLUE+)
(fl:color pbar fl:+WHITE+)
(fl:textcolor pbar fl:+RED+)

(fl:end window)

(fl:add-timeout 0.1 'ptimer (cffi:null-pointer))

(fl:show window)

(fl::run)
(quit)
