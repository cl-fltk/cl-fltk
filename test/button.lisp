(require :asdf)
(asdf:operate 'asdf:load-op 'cffi)
(asdf:operate 'asdf:load-op 'cl-fltk)

(defvar window)
(defvar b1)
(defvar b2)
(defvar b3)

(cffi:defcallback beepcb :void ((widget :pointer) (data :pointer))
		  (format t "beep!~%"))

(cffi:defcallback exitcb :void ((widget :pointer) (data :pointer))
		  (quit))

(setf window (fl:new-window 320 65 "bla"))
(fl:begin window)

(setf b1 (fl:new-button 20 20 80 25 "&Beep"))
(fl:callback b1 'beepcb 0)

(setf b2 (fl:new-button 120 20 80 25 "&no op"))

(setf b3 (fl:new-button 220 20 80 25 "E&xit"))
(fl:callback b3 'exitcb 0)

(fl:end window)
(fl:show window)

(fl::run)
(quit)
