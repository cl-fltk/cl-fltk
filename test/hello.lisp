(require :asdf)
(asdf:operate 'asdf:load-op 'cffi)
(asdf:operate 'asdf:load-op 'cl-fltk)

(defvar window)
(defvar box)

(setf window (cl-fltk:new-window 300 180 "bla"))
(cl-fltk:begin window)

(setf box (cl-fltk:new-widget 20 40 260 100 "Hello, World!"))

(cl-fltk:box box cl-fltk:+UP-BOX+)
(cl-fltk:labelfont box cl-fltk:+HELVETICA-BOLD-ITALIC+)
(cl-fltk:labelsize box 36.0)
(cl-fltk:labeltype box cl-fltk:+SHADOW-LABEL+)

(cl-fltk:end window)
(cl-fltk:show window)

(cl-fltk::run)
(quit)
