(in-package #:cl-fltk)

(defclass Group (Widget)
  ())

(defgeneric begin (group))

(defmethod begin ((group Group))
  (cffi:foreign-funcall "fl_group_begin" :pointer (foreign-object group)))

(defgeneric end (group))

(defmethod end ((group Group))
  (cffi:foreign-funcall "fl_group_end" :pointer (foreign-object group)))
