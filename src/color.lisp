(in-package #:cl-fltk)

(defconstant +NO_COLOR+ 0)
(defconstant +GRAY00+ 32)
(defconstant +GRAY05+ 33)
(defconstant +GRAY10+ 34)
(defconstant +GRAY15+ 35)
(defconstant +GRAY20+ 36)
(defconstant +GRAY25+ 37)
(defconstant +GRAY30+ 38)
(defconstant +GRAY33+ 39)
(defconstant +GRAY35+ 40)
(defconstant +GRAY40+ 41)
(defconstant +GRAY45+ 42)
(defconstant +GRAY50+ 43)
(defconstant +GRAY55+ 44)
(defconstant +GRAY60+ 45)
(defconstant +GRAY65+ 46)
(defconstant +GRAY66+ 47)
(defconstant +GRAY70+ 48)
(defconstant +GRAY75+ 49)
(defconstant +GRAY80+ 50)
(defconstant +GRAY85+ 51)
(defconstant +GRAY90+ 53)
(defconstant +GRAY95+ 54)
(defconstant +GRAY99+ 55)
(defconstant +BLACK+ #x38)
(defconstant +RED+ #x58)
(defconstant +GREEN+ #x3F)
(defconstant +YELLOW+ #x5F)
(defconstant +BLUE+ #xD8)
(defconstant +MAGENTA+ #xF8)
(defconstant +CYAN+ #xDF)
(defconstant +WHITE+ #xFF)
(defconstant +WINDOWS_BLUE+ #x88)

;inline Color color(unsigned char r, unsigned char g, unsigned char b) {
;FL_API Color color(const char*);
;FL_API Color lerp(Color c0, Color c1, float f);
;FL_API Color inactive(Color);
;FL_API Color inactive(Color, Flags f);
;FL_API Color contrast(Color fg, Color bg);
;FL_API void split_color(Color c, unsigned char& r, unsigned char& g, unsigned char& b);
;FL_API void set_color_index(Color index, Color);
;FL_API Color get_color_index(Color index);
;FL_API void set_background(Color);
;FL_API Color nearest_index(Color);
