(in-package #:cl-fltk)

(defun ask (text)
    (cffi:foreign-funcall "fl_ask_ask"
			  :pointer (cffi:foreign-string-alloc text)
			  :int))
