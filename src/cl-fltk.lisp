(in-package #:cl-fltk)

(defclass cl-fltk-object ()
  ((foreign-object :accessor foreign-object)))

(cffi:define-foreign-library libcl-fltk
    (:darwin "wrapper/libcl-fltk.so")
    (:unix "wrapper/libcl-fltk.so"))
(cffi:load-foreign-library 'libcl-fltk)
