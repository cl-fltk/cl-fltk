(in-package #:cl-fltk)

(defun run ()
  (cffi:foreign-funcall "fl_run"))

(defun add-timeout (timeout handler v)
  (cffi:foreign-funcall "fl_run_add_timeout"
			:float timeout
			:pointer (cffi:foreign-alloc :pointer :initial-element (cffi:get-callback handler))
			:pointer v))
