(in-package #:cl-fltk)

(defclass Window (Group)
  ())

(defun new-window (width height text)
  (let ((window-instance (make-instance 'Window )))
    (setf (foreign-object window-instance)
	  (cffi:foreign-funcall "new_window"
				:int width
				:int height
				:pointer (cffi:foreign-string-alloc text) :pointer))
    window-instance))

(defmethod show ((window Window))
  (cffi:with-foreign-object (argv :pointer 1)
			    (setf (cffi:mem-ref argv :pointer 0)
				  (cffi:foreign-string-alloc "cl-fltk")) ;argv
			    (cffi:foreign-funcall "fl_window_show"
						  :pointer (cl-fltk:foreign-object window)
						  :int 1 ;argc
						  :pointer argv)))