(in-package #:cl-fltk)

(defclass ProgressBar (Widget)
  ())

(defun new-progressbar (x y width height lbl)
  (let ((progressbar-instance (make-instance 'ProgressBar )))
    (setf (slot-value progressbar-instance 'foreign-object)
	  (cffi:foreign-funcall "new_progressbar"
				:int x
				:int y
				:int width
				:int height
				:pointer (cffi:foreign-string-alloc lbl) :pointer))
    progressbar-instance))

;;min int max int should be double
(defmethod range ((pb ProgressBar) (min double-float) (max double-float) &optional (step 1.0D0))
  (cffi:foreign-funcall "fl_progressbar_range"
			:pointer (cl-fltk:foreign-object pb)
			:double min
			:double max
			:double step))
;;step is allready used as function name so here is renamed to preogressbar-step
(defmethod progressbar-step ((pb ProgressBar) &optional step)
  (if step
      (cffi:foreign-funcall "fl_progressbar_step"
			    :pointer (cl-fltk:foreign-object pb)
			    :double step)
      (cffi:foreign-funcall "fl_progressbar_get_step"
			    :pointer (cl-fltk:foreign-object pb))))

(defmethod progressbar-position ((pb ProgressBar) &optional position)
  (if position
      (cffi:foreign-funcall "fl_progressbar_position"
			    :pointer (cl-fltk:foreign-object pb)
			    :double position))
      (cffi:foreign-funcall "fl_progressbar_get_position"
			    :pointer (cl-fltk:foreign-object pb) :double))

(defmethod progressbar-minimum ((pb ProgressBar) &optional nm)
  (if nm
      (cffi:foreign-funcall "fl_progressbar_minimum"
			    :pointer (cl-fltk:foreign-object pb)
			    :double nm))
      (cffi:foreign-funcall "fl_progressbar_get_minimum"
			    :pointer (cl-fltk:foreign-object pb) :double))

(defmethod progressbar-maximum ((pb ProgressBar) &optional nm)
  (if nm
      (cffi:foreign-funcall "fl_progressbar_maximum"
			    :pointer (cl-fltk:foreign-object pb)
			    :double nm))
      (cffi:foreign-funcall "fl_progressbar_get_maximum"
			    :pointer (cl-fltk:foreign-object pb) :double))

(defmethod progressbar-showtext ((pb ProgressBar) &optional flag)
  (if flag
      (cffi:foreign-funcall "fl_progressbar_showtext"
			    :pointer (cl-fltk:foreign-object pb)
			    :boolean flag))
      (cffi:foreign-funcall "fl_progressbar_get_showtext"
			    :pointer (cl-fltk:foreign-object pb) :boolean))

(defmethod progressbar-text-color ((pb ProgressBar) &optional color)
  (if color
      (cffi:foreign-funcall "fl_progressbar_text_color"
			    :pointer (cl-fltk:foreign-object pb)
			    :int color))
      (cffi:foreign-funcall "fl_progressbar_get_text_color"
			    :pointer (cl-fltk:foreign-object pb) :int))
