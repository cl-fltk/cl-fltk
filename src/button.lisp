(in-package #:cl-fltk)

(defclass Button (Widget)
  ())

(defun new-button (x y width height text)
  (let ((button-instance (make-instance 'Button )))
    (setf (slot-value button-instance 'foreign-object)
	  (cffi:foreign-funcall "new_button"
				:int x
				:int y
				:int width
				:int height
				:pointer (cffi:foreign-string-alloc text) :pointer))
    button-instance))
