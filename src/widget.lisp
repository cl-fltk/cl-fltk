(in-package #:cl-fltk)

(defclass Widget (cl-fltk-object)
  ())

(defconstant +RESERVED-TYPE+ #x64)
(defconstant +GROUP-TYPE+ #xE0)
(defconstant +WINDOW-TYPE+ #xF0)

(defun new-widget (x y width height text)
  (let ((widget-instance (make-instance 'Widget )))
    (setf (foreign-object widget-instance)
	  (cffi:foreign-funcall "new_widget"
				:int x
				:int y
				:int width
				:int height
				:pointer (cffi:foreign-string-alloc text) :pointer))
    widget-instance))

(defgeneric show (widget))

(defmethod show ((widget Widget))
  (cffi:foreign-funcall "fl_widget_show"
			:pointer (cl-fltk:foreign-object widget)))

(defun hide (widget)
  (cffi:foreign-funcall "fl_widget_hide"
			:pointer (cl-fltk:foreign-object widget)))
  
;;TODO :pointer data -> :string data -> :int data etc.
(defmethod callback ((widget Widget) (callback-function symbol) &optional (data (cffi:null-pointer)))
  (cffi:foreign-funcall "callback"
			:pointer (cl-fltk:foreign-object widget)
			:pointer (cffi:get-callback callback-function)
			:pointer data))

(defmethod box ((widget widget) box)
  (cffi:foreign-funcall "fl_widget_box"
			:pointer (cl-fltk:foreign-object widget)
			:pointer box))

(defmethod labelfont ((widget Widget) font)
  (cffi:foreign-funcall "fl_widget_labelfont"
			:pointer (cl-fltk:foreign-object widget)
			:pointer font))

(defmethod labeltype ((widget Widget) type)
  (cffi:foreign-funcall "fl_widget_labeltype"
			:pointer (cl-fltk:foreign-object widget)
			:pointer type))

(defmethod labelsize ((widget Widget) &optional size)
  (if size
      (cffi:foreign-funcall "fl_widget_labelsize"
			    :pointer (cl-fltk:foreign-object widget)
			    :float size)
      (cffi:foreign-funcall "fl_widget_get_labelsize"
			    :pointer (cl-fltk:foreign-object widget) :float)))

(defmethod clear-flag ((widget Widget) (flag integer))
  (cffi:foreign-funcall "fl_widget_clear_flag"
			:pointer (cl-fltk:foreign-object widget)
			:int flag))

(defmethod set-flag ((widget Widget) (flag integer))
  (cffi:foreign-funcall "fl_widget_set_flag"
			:pointer (cl-fltk:foreign-object widget)
			:int flag))

(defmethod selection-color ((widget Widget) &optional color)
  (if color
      (cffi:foreign-funcall "fl_widget_selection_color"
			    :pointer (cl-fltk:foreign-object widget)
			    :int color)
      (cffi:foreign-funcall "fl_widget_get_selection_color"
			    :pointer (cl-fltk:foreign-object widget) :int )))

(defmethod color ((widget Widget) &optional color)
  (if color
      (cffi:foreign-funcall "fl_widget_color"
			    :pointer (cl-fltk:foreign-object widget)
			    :int color)
      (cffi:foreign-funcall "fl_widget_get_color"
			    :pointer (cl-fltk:foreign-object widget) :int)))

(defmethod textcolor ((widget Widget) color)
  (if color
      (cffi:foreign-funcall "fl_widget_textcolor"
			    :pointer (cl-fltk:foreign-object widget)
			    :int color)
      (cffi:foreign-funcall "fl_widget_get_textcolor"
			    :pointer (cl-fltk:foreign-object widget) :int)))

(defmethod buttonbox ((widget Widget) box)
  (cffi:foreign-funcall "fl_widget_buttonbox"
			:pointer (cl-fltk:foreign-object widget)
			:pointer box))

(defmethod focusbox ((widget Widget) box)
  (cffi:foreign-funcall "fl_widget_focusbox"
			:pointer (cl-fltk:foreign-object widget)
			:pointer box))

(defmethod textfont ((widget Widget) font)
  (cffi:foreign-funcall "fl_widget_textfont"
			:pointer (cl-fltk:foreign-object widget)
			:pointer font))

(defmethod selection-textcolor ((widget Widget) &optional color)
  (if color
      (cffi:foreign-funcall "fl_widget_selection_textcolor"
			    :pointer (cl-fltk:foreign-object widget)
			    :int color)
      (cffi:foreign-funcall "fl_widget_get_selection_textcolor"
			    :pointer (cl-fltk:foreign-object widget) :int)))

(defmethod buttoncolor ((widget Widget) &optional color)
  (if color
      (cffi:foreign-funcall "fl_widget_buttoncolor"
			    :pointer (cl-fltk:foreign-object widget)
			    :int color)
      (cffi:foreign-funcall "fl_widget_get_buttoncolor"
			    :pointer (cl-fltk:foreign-object widget) :int)))

(defmethod labelcolor ((widget Widget) &optional color)
  (if color
      (cffi:foreign-funcall "fl_widget_labelcolor"
			    :pointer (cl-fltk:foreign-object widget)
			    :int color)
      (cffi:foreign-funcall "fl_widget_get_labelcolor"
			    :pointer (cl-fltk:foreign-object widget) :int)))

(defmethod highlight-color ((widget Widget) &optional color)
  (if color
      (cffi:foreign-funcall "fl_widget_highlight_color"
			    :pointer (cl-fltk:foreign-object widget)
			    :int color)
      (cffi:foreign-funcall "fl_widget_get_highlight_color"
			    :pointer (cl-fltk:foreign-object widget) :int)))

(defmethod highlight-textcolor ((widget Widget) &optional color)
  (if color
      (cffi:foreign-funcall "fl_widget_highlight_textcolor"
			    :pointer (cl-fltk:foreign-object widget)
			    :int color)
      (cffi:foreign-funcall "fl_widget_get_highlight_textcolor"
			    :pointer (cl-fltk:foreign-object widget) :int)))

(defmethod textsize ((widget Widget) &optional size)
  (if size
      (cffi:foreign-funcall "fl_widget_textsize"
			    :pointer (cl-fltk:foreign-object widget)
			    :float size)
      (cffi:foreign-funcall "fl_widget_get_textsize"
			    :pointer (cl-fltk:foreign-object widget) :float)))

(defmethod leading ((widget Widget) &optional leading)
  (if leading
      (cffi:foreign-funcall "fl_widget_leading"
			    :pointer (cl-fltk:foreign-object widget)
			    :float leading)
      (cffi:foreign-funcall "fl_widget_get_leading"
			    :pointer (cl-fltk:foreign-object widget) :float)))

(defmethod scrollbar-align ((widget Widget) &optional c)
  (if c
      (cffi:foreign-funcall "fl_widget_scrollbar_align"
			    :pointer (cl-fltk:foreign-object widget)
			    :unsigned-char c)
      (cffi:foreign-funcall "fl_widget_get_scrollbar_align"
			:pointer (cl-fltk:foreign-object widget) :unsigned-char)))

(defmethod scrollbar-width ((widget Widget) &optional c)
  (if c
      (cffi:foreign-funcall "fl_widget_scrollbar_width"
			    :pointer (cl-fltk:foreign-object widget)
			    :unsigned-char c)
      (cffi:foreign-funcall "fl_widget_get_scrollbar_width"
			    :pointer (cl-fltk:foreign-object widget) :unsigned-char)))

(defmethod send ((widget Widget) event)
  (cffi:foreign-funcall "fl_widget_send"
			:pointer (cl-fltk:foreign-object widget)
			:int event :int))
